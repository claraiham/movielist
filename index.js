const movieList = [
    {
      title: "Avatar",
      releaseYear: 2009,
      duration: 162,
      director: "James Cameron",
      actors: ["Sam Worthington","Zoe Saldana", "Sigourney Weaver", "Stephen Lang"],
      description: "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
      poster: "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg",
      rating: 7.9,
    },
    {
      title: "300",
      releaseYear: 2006,
      duration: 117,
      director: "Zack Snyder",
      actors: ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],
      description: "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
      poster: "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg",
      rating: 7.7,
    },
    {
      title: "The Avengers",
      releaseYear: 2012,
      duration: 143,
      director: "Joss Whedon",
      actors: ["Robert Downey Jr.", "Chris Evans", "Mark Ruffalo", "Chris Hemsworth"],
      description: "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
      poster: "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
      rating: 8.1,
    },

  ];


// je crée une div
const listcContent = document.createElement("div");


// je lui donne une classe
listcContent.classList.add("grid");

function minToHour(duration){
  let hours = parseInt(duration/60);
  let minutes = duration % 60;
  return `${hours}h ${minutes}`;
}


function displayMovie (){
  for(const movie of movieList){
    addOneMovie(movie)
  }
  
  document.body.appendChild(listcContent);
}
displayMovie(); // au démarrage j'affiche la liste des films

function addOneMovie(movie){
    const movies = document.createElement("ul");// création d'une liste pour que mes éléments s'affichent sous forme de liste
    const movieImg = document.createElement("img");// pour recevoir l'image
    const movieTitle = document.createElement("h1");// Balise titre
    //balises <p> pour du texte
    const movieDescription = document.createElement("p");
    const movieDirector = document.createElement("p");
    const movieDuration = document.createElement("p");
    const movieYear = document.createElement("p");
    const movieActors = document.createElement("p");


    const deleteButton = document.createElement("button");
    const textButton = document.createTextNode("Delete");
    deleteButton.appendChild(textButton);
    deleteButton.addEventListener("click", function(){
      listcContent.removeChild(movies);
      const index = movieList.indexOf(movie); // récupère l'index du film 
      movieList.splice(index, 1); // retire le film à l'index donné
    } );
  
    movieImg.src = movie.poster;// je vais chercher l'image via le chemin d'accès donné dans le tableau
    movieTitle.append(movie.title);// j'ajoute le titre dans mon élément h1
    movieYear.append(movie.releaseYear);// j'ajoute le titre dans mon élément p
  
    movieActors.append(movie.actors);
  
    movieDuration.append(minToHour(movie.duration));// j'ajoute le titre dans mon élément p
    movieDirector.append(movie.director);// j'ajoute le titre dans mon élément p
    movieDescription.append(movie.description);// j'ajoute le titre dans mon élément p
  
    movies.appendChild(movieImg);// j'ajoute les éléments dans les div
    movies.appendChild(movieTitle);
    movies.appendChild(movieActors);
    movies.appendChild(movieDirector);
    movies.appendChild(movieDescription);
    movies.appendChild(movieDuration);
    movies.appendChild(movieYear);
    movies.appendChild(deleteButton);
  
    listcContent.appendChild(movies);// j'ajoute des div dans ma div
}


//Je récupère le bouton, lui dit que j'appelle la fonction addMovie quand je clique dessus
const submitForm = document.getElementById("submitForm");



// je récupère  ce qui a été saisi dans le form
const form = document.querySelector('.addMovieForm');

submitForm.addEventListener("click", addMovie );


function addMovie(event){
  //empecher le rafraichissement de la page 
  event.preventDefault();

  // récupérer les données
  const titleValue = form.title.value;
  console.log(titleValue);

  const releasedYearValue = form.releasedYear.value;
  console.log(releasedYearValue);

  const durationValue = form.duration.value;
  console.log(durationValue);

  const directorValue = form.director.value;
  console.log(directorValue);

  const actorsValue = form.actors.value;
  console.log(actorsValue);

  const descriptionValue = form.description.value;
  console.log(descriptionValue);

  const posterValue = form.poster.value;
  console.log(posterValue);

  const ratingValue = form.rating.value;
  console.log(ratingValue);


  //créer un objet littéral
  const newMovie = {
      title: titleValue,
      releaseYear: releasedYearValue,
      duration: durationValue,
      director: directorValue,
      actors: [actorsValue],
      description: descriptionValue,
      poster: posterValue,
      rating: ratingValue,
  }

  // ajouter les données récupérées, au tableau

  movieList.push(newMovie)

  addOneMovie(newMovie)

}

console.log(movieList)